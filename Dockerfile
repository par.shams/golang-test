FROM golang:alpine as builder
WORKDIR /src
COPY . .
RUN GO111MODULE=on CGO_ENABLED=0 go build -a -installsuffix cgo \
    -ldflags "-w -X main.Version=${test}" \
    -i -o ./build/test  main.go
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /app
COPY --from=builder /src/build/ ./
ENTRYPOINT [ "./test" ]
